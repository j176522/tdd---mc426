/*
 * Alunos:
 * 	Jonathas Sardinha
 * 	Eduardo Yuji
 */

package shotgun;

import java.util.concurrent.TimeUnit;

public class Countdown {
	
	private int time;
	private boolean stopped = false;
	
	public boolean stopped() {
		return stopped;
	}

	public void start(int seconds) {
		time = seconds;
		for(; seconds > 0; seconds --) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			decrease(1);
		}
	}
	
	private void decrease(int amount) {
		time -= amount;
		if (time == 0) stopped = true;
	}
	
}
