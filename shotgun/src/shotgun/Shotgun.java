/*
 * Alunos:
 * 	Jonathas Sardinha
 * 	Eduardo Yuji
 */

package shotgun;

public class Shotgun {

	
	private int magazine;
	private int bullets;
	private boolean isReloading = false;

	/**
	 * 
	 * @param magazine Size of magazine
	 */
	public Shotgun(int magazine) {
		this.magazine = magazine;
		this.bullets = magazine;
	}
	
	/**
	 * 
	 * @return True if weapon is reloading, else returns False
	 */
	public boolean isReloading() {
		return isReloading;
	}
	
	/**
	 * Shoots weapon and removes a bullet from the magazine.
	 * If the magazine empties reloads the weapon and renders it
	 * unusable until this process finishes
	 */
	public void shoot() {
		if (!isReloading) bullets--;
		if (bullets == 0 && !isReloading) {
			isReloading = true;
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					Countdown clock = new Countdown();
					if(clock.stopped()) {
						clock.start(2);
						while(!clock.stopped()) {}
					}
					bullets = magazine;
					isReloading = false;
				}
			}).start();
			
			
		}

	}

	public int getMagazine() {
		return magazine;
	}

	public void setMagazine(int magazine) {
		this.magazine = magazine;
	}

	public int getBullets() {
		return bullets;
	}

	public void setBullets(int bullets) {
		this.bullets = bullets;
	}
	
	
	
	
}
