/*
 * Alunos:
 * 	Jonathas Sardinha
 * 	Eduardo Yuji
 */

package testes;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import shotgun.Shotgun;

public class Testes {
	
	@Test
	public void testIfGunIsCreated() {
		Shotgun gun = new Shotgun(5);
		assertEquals(5, gun.getMagazine());
		assertEquals(5, gun.getBullets());
	}
	
	@Test
	public void testIfShoots() {
		Shotgun gun = new Shotgun(2);
		gun.shoot();
		assertEquals(1, gun.getBullets());
	}
	
	@Test
	public void testIfReloads() {
		Shotgun gun = new Shotgun(1);
		gun.shoot();
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(1, gun.getBullets());
	}
	
	@Test
	public void testIfIsReloading() {
		Shotgun gun = new Shotgun(1);
		assertEquals(false, gun.isReloading());
		gun.shoot();
		assertEquals(true, gun.isReloading());
	}
	
	@Test
	public void testIfStopsReloading() {
		Shotgun gun = new Shotgun(1);
		gun.shoot();
		assertEquals(true, gun.isReloading());
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(false, gun.isReloading());
	}
	
	@Test
	public void testIfShootsWhileReloading() {
		Shotgun gun = new Shotgun(1);
		gun.shoot();
		assertEquals(true, gun.isReloading());
		gun.shoot();
		assertEquals(0, gun.getBullets());
	}

}
