/*
 * Alunos:
 * 	Jonathas Sardinha
 * 	Eduardo Yuji
 */

package cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Cache {
	
	private final static long SECONDS_TO_TIMEOUT = 2;
	private final static int STANDARD_SIZE= 10;
	
	//Contains key-value pairs
	private Map<String, String> items;
	//List of time since last access to a certain key
	private List<Times> times;
	//Size of cache (in items)
	private int size;
	
	public Cache() {
		this.items = new HashMap<String, String>();
		this.times = new ArrayList<Times>();
		this.size = STANDARD_SIZE;
		//Starts a new thread that checks how many seconds an item has been inactive
		new Thread(new Runnable() {
			
			@Override
			public synchronized void run() {
				while(true) {
					times = times
							.stream()
							//Adds 1 to each second of the time passed since access
							//to every item in the cache
							//If the time since last access is higher than the timeout
							//then removes the item from the map and returns null
							.map(time -> {
								time.setTimePassed(time.getTimePassed()+1);
								if (time.getTimePassed() < SECONDS_TO_TIMEOUT) {
									return time;
								} else {
									items.remove(time.getKey());
									return null;
								}
								
							})
							//removes all items that are null from the list
							.filter(time -> time != null)
							.collect(Collectors.toList());
					
					try{
						TimeUnit.SECONDS.sleep(1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
			
		}).start();
	}
	
	/**
	 * 
	 * @param size Size of cache in items
	 */
	public Cache(int size) {
		this.items = new HashMap<String, String>();
		this.times = new ArrayList<Times>();
		this.size = size;
		
		//Same as thread from other constructor
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					
					times = times
							.stream()
							.map(time -> {
								time.setTimePassed(time.getTimePassed()+1);
								if (time.getTimePassed() < SECONDS_TO_TIMEOUT) {
									return time;
								} else {
									items.remove(time.getKey());
									return null;
								}
								
							})
							.filter(time -> time != null)
							.collect(Collectors.toList());
					
					try{
						TimeUnit.SECONDS.sleep(1);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
			
		}).start();
	}
	
	/**
	 * 
	 * @param key Key in key-value pair
	 * @param value Value in key-value pair
	 */
	public void addItem(String key, String value) {
		if (items.size() == size) {
			//removes item with higher time since access
			//(treats the list of times as queue)
			Times leastUsed = times.remove(times.size()-1);
			items.remove(leastUsed.getKey());
		}
		items.put(key, value);
		Times newItem = new Times(key);
		times.add(0, newItem);
	}
	
	public int numberOfItems() {
		return items.size();
	}
	
	/**
	 * 
	 * @param key Key to search in key-value pair
	 * @return Returns value in key-value pair
	 */
	public String getItem(String key) {
		Times toStart = new Times(key);
		//re-inserts the key in the queue with time 0
		times.remove(toStart);
		times.add(0, toStart);
		return items.get(key);
	}
	
	/**
	 * 
	 * @return Size of cache in items
	 */
	public int getSize() {
		return size;
	}
	
	public class Times {
		String key;
		long timePassed;
		
		public Times (String key) {
			this.key = key;
			timePassed = 0;
		}
		
		public String getKey() {
			return key;
		}



		public void setKey(String key) {
			this.key = key;
		}



		public long getTimePassed() {
			return timePassed;
		}



		public void setTimePassed(long timePassed) {
			this.timePassed = timePassed;
		}



		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getEnclosingInstance().hashCode();
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Times other = (Times) obj;
			if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
				return false;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}
		private Cache getEnclosingInstance() {
			return Cache.this;
		}		
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + size;
		result = prime * result + ((times == null) ? 0 : times.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cache other = (Cache) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (size != other.size)
			return false;
		if (times == null) {
			if (other.times != null)
				return false;
		} else if (!times.equals(other.times))
			return false;
		return true;
	}
	
		

}
