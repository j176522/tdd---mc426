/*
 * Alunos:
 * 	Jonathas Sardinha
 * 	Eduardo Yuji
 */

package testes;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import cache.Cache;

public class Testes {
	
	@Test
	public void testCreateCache() {
		Cache cache = new Cache();
		assertEquals(0, cache.numberOfItems());
	}
	
	@Test
	public void testNumberAfterAddItem() {
		Cache cache = new Cache();
		cache.addItem("newItem", "newItem");
		assertEquals(1, cache.numberOfItems());
	}
	
	@Test
	public void testItemAfterAddOneItem() {
		Cache cache = new Cache();
		cache.addItem("newKey", "newValue");
		assertEquals("newValue", cache.getItem("newKey"));
	}
	
	@Test
	public void testItemAfterAddTwoItems() {
		Cache cache = new Cache();
		cache.addItem("key1", "value1");
		cache.addItem("key2", "value2");
		assertEquals("value1", cache.getItem("key1"));
	}
	
	@Test
	public void testCacheWithSize() {
		Cache cache = new Cache(5);
		assertEquals(5, cache.getSize());
	}
	
	@Test
	public void testAddMoreThanSize() {
		Cache cache = new Cache(1);
		cache.addItem("key1", "value1");
		cache.addItem("key2", "value2");
		assertEquals(1, cache.numberOfItems());
	}
	
	@Test
	public void testItemAfterSizeOverflow() {
		Cache cache = new Cache(1);
		cache.addItem("key1", "value1");
		cache.addItem("key2", "value2");
		assertEquals("value2", cache.getItem("key2"));
	}
	
	@Test
	public void testAddItemAfterLeastUsedChanged() {
		Cache cache = new Cache(2);
		cache.addItem("key1", "value1");
		cache.addItem("key2", "value2");
		cache.getItem("key1");
		cache.addItem("key3", "value3");
		assertEquals(null, cache.getItem("key2"));
	}
	
	@Test
	public void testTimeoutItem() {
		Cache cache = new Cache(1);
		cache.addItem("key", "value");
		assertEquals("value", cache.getItem("key"));
		try{
				TimeUnit.SECONDS.sleep(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(null, cache.getItem("key"));
	}

}
